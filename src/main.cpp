#include <iostream>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>
#include <random>
#include <omp.h>

#include "MersenneTwister.h"
#include "CStopWatch.h"

using namespace std;
#define PI_F 3.141592654f 

typedef vector<int> 		iArray1D;
typedef vector<double> 		ldArray1D;
typedef vector<ldArray1D> 	ldArray2D;
typedef vector<ldArray2D> 	ldArray3D;

int Nd;
MTRand mt;
int xMin = -5.12;
int xMax = 5.12;
int vMin = -1; 
int vMax = 1;
int Np;
float C1  = 2.05, C2 = 2.05;
float R1;
float R2;
float w;
ldArray2D pBestPosition;
ldArray1D gBestPosition;
const int threadMin = 1;
int numThreads;
const int threadMax = 6;
ldArray2D R;
ldArray2D V;
ldArray1D M;
int numEvals;
std::random_device rd;    
double Rastrigin(ldArray2D& R, int Nd, int p) { // Rastrigin
	double Z=0,  Xi;

	for(int i=0; i<Nd; i++){
		Xi = R[p][i];
		Z += (pow(Xi,2) - 10 * cos(2*PI_F*Xi) + 10);
	}
	return -Z;
}
// only used once to initialise (seed) engine
double randDbl(const double& min, const double& max) {
    static thread_local mt19937* generator = nullptr;
    if (!generator) {
        generator = new mt19937(clock() + omp_get_thread_num());
    }
    uniform_real_distribution<double> distribution(min, max);
    return distribution(*generator);
}
void UpdatePosition()
{
    int frow, lrow, m;
	int my_rank = omp_get_thread_num(); 
    numThreads = omp_get_num_threads(); 
    if (Np%numThreads == 0){
        m = Np/numThreads;
        frow = (((my_rank)) * m);
        lrow =((((my_rank)+1))* m - 1);
    }
    else
    {
        if (my_rank != numThreads - 1)
        {
            m = std::floor(Np/numThreads);
            frow = (((my_rank)) * m);
            lrow =((((my_rank)+1))* m - 1);
        }
        else{
            m = Np - (std::floor(Np/numThreads) * (numThreads));
            frow = my_rank - m;
            lrow = Np - 1;
        }
      }
	for (int r = frow; r <= lrow; r++)
	{
		for(int i=0; i<Nd; i++){
		R[r][i] = R[r][i] + V[r][i];

		if(R[r][i] > xMax) R[r][i] = xMin + randDbl(xMin, xMax);
        if(R[r][i] < xMin) R[r][i] = xMin + randDbl(xMin, xMax);
		}
	}
}

void EvaluateFitness()
{
    int frow, lrow, m;
	int my_rank = omp_get_thread_num(); 
    numThreads = omp_get_num_threads(); 
    if (Np%numThreads == 0){
        m = Np/numThreads;
        frow = (((my_rank)) * m);
        lrow =((((my_rank)+1))* m - 1);
    }
    else
    {
        if (my_rank != numThreads - 1)
        {
            m = std::floor(Np/numThreads);
            frow = (((my_rank)) * m);
            lrow =((((my_rank)+1))* m - 1);
        }
        else{
            m = Np - (std::floor(Np/numThreads) * (numThreads));
            frow = my_rank - m;
            lrow = Np - 1;
        }
      }
	for (int r = frow; r <= lrow; r++)
	{
		M[r] = Rastrigin(R, Nd, r);
		 numEvals++;
	}
}

void InitPopulation()
{
    int frow, lrow, m;
	int my_rank = omp_get_thread_num(); 
    numThreads = omp_get_num_threads();
    if (Np%numThreads == 0){
        m = Np/numThreads;
        frow = (((my_rank)) * m);
        lrow =((((my_rank)+1))* m - 1);
    }
    else
    {
        if (my_rank != numThreads - 1)
        {
            m = std::floor(Np/numThreads);
            frow = (((my_rank)) * m);
            lrow =((((my_rank)+1))* m - 1);
        }
        else{
            m = Np - (std::floor(Np/numThreads) * (numThreads));
            frow = my_rank - m;
            lrow = Np - 1;
        }
      }
	for (int r = frow; r <= lrow; r++)
	{
		for(int i=0; i<Nd; i++){
			R[r][i] = xMin + randDbl(xMin, xMax);
			V[r][i] = vMin + randDbl(vMin, vMax);
			if(rand() < 0.5){
				R[r][i] = -(R[r][i]);
				V[r][i] = -(V[r][i]);
			}
		}
	}
}

void UpdateVelocity()
{
    int frow, lrow, m;
	int my_rank = omp_get_thread_num(); 
    numThreads = omp_get_num_threads();
    if (Np%numThreads == 0){
        m = Np/numThreads;
        frow = (((my_rank)) * m);
        lrow =((((my_rank)+1))* m - 1);
    }
    else
    {
        if (my_rank != numThreads - 1)
        {
            m = std::floor(Np/numThreads);
            frow = (((my_rank)) * m);
            lrow =((((my_rank)+1))* m - 1);
        }
        else{
            m = Np - (std::floor(Np/numThreads) * (numThreads));
            frow = my_rank - m;
            lrow = Np - 1;
        }
      }
	for (int r = frow; r <= lrow; r++)
	{
		for(int i=0; i<(Nd); i++){
			R1 = rand(); 
			R2 = rand();

			// Original rSO
			V[r][i] = w * V[r][i] + C1*R1*(pBestPosition[r][i] - R[r][i]) + C2*R2*(gBestPosition[i] - R[r][i]);
			if(V[r][i] > vMax) V[r][i] = vMin + randDbl(vMin, vMax);
			if(V[r][i] < vMin) V[r][i] = vMin + randDbl(vMin, vMax);
		}
	}
}

void PSO(int Np, int Nd, int Nt, float xMin, float xMax, float vMin, float vMax, 
			double (*objFunc)(ldArray2D& ,int, int), int& numEvals, string functionName){
	ldArray2D pBestPositionLoc(Np, vector < double > (Nd,-INFINITY));
	ldArray1D pBestValue(Np,-INFINITY);
	vector < vector < double > > RLoc(Np, vector < double > (Nd, 0));
	vector < vector < double > > VLoc(Np, vector < double > (Nd, 0));
	vector < double > MLoc(Np,0);
	ldArray1D gBestPositionLoc(Nd, -INFINITY);
	R = RLoc;
	V = VLoc;
	M = MLoc;
	pBestPosition = pBestPositionLoc;
	gBestPosition = gBestPositionLoc;
	numEvals = 0;
    float gBestValue = -INFINITY;
	int lastStep = Nt, bestTimeStep;
	
	float wMax = 0.9, wMin = 0.1;

	CStopWatch timer, timer1;
	float positionTime = 0, fitnessTime = 0, velocityTime = 0, totalTime = 0;;
    timer1.startTimer();
    
	// Init Population
       #pragma omp parallel num_threads(numThreads)
        InitPopulation();
	// Evaluate Fitness
		#pragma omp parallel num_threads(numThreads)
        EvaluateFitness();
	for(int j=1; j<(Nt); j++){

		//Update Positions
		timer.startTimer();
        #pragma omp parallel num_threads(numThreads)
        UpdatePosition();

		timer.stopTimer();
		positionTime += timer.getElapsedTime();

		// Evaluate Fitness
		timer.startTimer();
		#pragma omp parallel num_threads(numThreads)
        EvaluateFitness();
		for(int p=0; p<Np; p++){
		    if(M[p] > gBestValue){
				gBestValue = M[p];
				for(int i=0; i<(Nd); i++){
				    gBestPosition[i] = R[p][i];
				}
				bestTimeStep = j;
			}
			
			// Local
		    if(M[p] > pBestValue[p]){
			    pBestValue[p] = M[p];
                for(int i=0; i<(Nd); i++){
				    pBestPosition[p][i] = R[p][i];
				}
			}
			
		}
		timer.stopTimer();
		fitnessTime += timer.getElapsedTime();

        if(gBestValue >= -0.0001){
            lastStep = j;
    	    break;
	    }

		// Update Velocities
		timer.startTimer();
		w = wMax - ((wMax-wMin)/Nt) * j;
	    #pragma omp parallel num_threads(numThreads)
        UpdateVelocity();

		timer.stopTimer();
		velocityTime += timer.getElapsedTime();
	} // End Time Steps

    timer1.stopTimer();
    totalTime += timer1.getElapsedTime();
	
	R.clear(); V.clear(); M.clear();
  	pBestPosition.clear(); pBestValue.clear(); gBestPosition.clear();
	cout    << functionName << " "
            << gBestValue   << " " 
            << Np    		<< " "
            << Nd           << " "
            << lastStep     << " "
            << numEvals     << " "
            << positionTime << " "
            << fitnessTime  << " "
            << velocityTime << " "
            << numThreads << endl;
}

void runPSO(double xMin, double xMax, double vMin, double vMax, 
			double (*rPtr)(ldArray2D& , int, int), string functionName){
	CStopWatch overallTimer;			
    int Nt;
    int NdMin, NdMax, NdStep;
	int NpMin, NpMax, NpStep;
    float overallTime;
    NdMin = 10; NdMax = 50;  NdStep = 20;
	NpMin = 50; NpMax = 500; NpStep = 50; 
    Nt = 3000; Nd = 30;
    overallTimer.startTimer();
    for(numThreads=threadMin; numThreads <= threadMax; numThreads++)
    {
        for(Np=NpMin; Np<=NpMax; Np+=NpStep){
		    for(Nd=NdMin; Nd<=NdMax; Nd+=NdStep){
			    for(int curTrial=0; curTrial<10; curTrial++){
				    PSO(Np, Nd, Nt, xMin, xMax, vMin, vMax, rPtr, numEvals, functionName);
			    }
			    cout << endl;
		    }
	    }
    }
	overallTimer.stopTimer();
	overallTime = overallTimer.getElapsedTime();
	cout << endl;
	cout << "Total Runtime: " << overallTime << std::endl;
}
int main(){

	double (*rPtr)(ldArray2D& , int, int) = NULL;
      
    cout << "Function, Fitness, Last Step, Np, Nd, Evals, Position Time, Fitness Time, Velocity Time, NumThreads" << endl;
    
    rPtr = &Rastrigin;
    runPSO(xMin, xMax, vMin, vMax, rPtr, "F2");
    
    rPtr = NULL;

	return 0;
}
