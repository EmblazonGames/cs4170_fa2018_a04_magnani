### OpenMP Assignment #2 for CS 4170/5170 ###
## Summary ##
This program is a parallelized version of the Particle Swarm Optimization Algorithm (PSO), written using OpenMP.
It makes use of the omp_parallel directive to divide work among the given number of threads. Currently,
the functions that are parallelized are: 


InitPopulation
UpdateVelocity
UpdatePosition


In each of the functions, calculations are performed to divvy up the work as evenly as possible between threads.

## Instructions ##
To compile and run from command line if you are not on windows:
```
cd src
g++ -fopenmp main.cpp CStopWatch.cpp
./a.out
```
or
```
cd Default && make all
```

To compile and run with Docker:
```
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp ./OpenMP
```

## Instructions: Using OSC ##
Move all the files to the Ohio Supercomputing Center (OSC) server of your choice. Make sure to build your code, then modify the `jobScript` accordingly. Submit from inside the `Default` directory using 
```
qsub jobScript
```

You may also do this using http://ondemand.osc.edu
You may also do this using http://ondemand.osc.edu

## Algorithm ##
"The Particle Swarm Optimization algorithm is comprised of a collection of particles that move around the search space influenced by their own best past location and the best past location of the whole swarm or a close neighbor."

From: http://www.cleveralgorithms.com/nature-inspired/swarm/pso.html 

## Method ##
To parallelize this algorithm, I first noted the functions that needed to be paralellized, which are listed above. Then, I moved the inner loops for the corresponding logic
from the PSO() function into their own functions. I then made the variables that those functions use global, after noting that most of the write operations were to arrays
and so there should not be any race conditions if each thread worked on a different section of the array. Then, I went and calcuated a formula for how to divide up the 
work between the threads. This formula ended up being: 

    m = std::floor(Np/numThreads);
    frow = (((my_rank)) * m);
    lrow =((((my_rank)+1))* m - 1);

Where m is the number each thread works on, frow is the first index and lrow is the index to stop at. If the last thread is working and the Np is not evenly divisble by m, 
then the last thread has a different formula:

    m = Np - (std::floor(Np/numThreads) * (numThreads));
    frow = my_rank - m;
    lrow = Np - 1;

Finally, after parallelizing the functions, I went and added in the omp_parallel directive with the number of threads, in place of the outer for loop in PSO().

## Discussion and Analysis ##

In the file PSO_Results.xlsx you will find my analysis of the results of the program, which I ran on OSC with numThreads from 1 to 6. 

Overall, the program had the best performance with numThreads=5, followed by numThreads=2. When there were 5 threads working an incredible boost in performance 
was seen, improving from 0.58 seconds on average to 0.27 seconds. However, if we take into account efficiencies, the improvement from 0.58 seconds to 0.51 seconds for
two processors was still more efficient per thread, than when using 5 threads.

The program showed a decrease in performance for numThreads= 3,4,6 and I believe this may be due to improper scheduling on my behalf, although I struggled to figure out where.

